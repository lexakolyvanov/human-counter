import smtplib
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders


def sendMessage(data, noize_graph_buf, human_graph_buf):
    password = "NMT-162701"
    msg = MIMEMultipart()
    msg['Subject'] = 'Отчет о количестве людей и уровне шума в аудитории фт-214'
    msg['From'] = 'vishvishvish0002@gmail.com'
    msg['To'] = 'belik-66@yandex.ru'

    body = MIMEText("""Максимальный уровень шума: """ + str(data['noise']['max']) + '\n'
                    """Минимальный уровень шума: """ + str(data['noise']['min']) + '\n\n'
                    """Максимальное количество людей: """ + str(data['human']['max']) + '\n'
                    """Минимальное количество людей: """ + str(data['human']['min']) + '\n'
                    """Суммарное количество людей: """ + str(data['human']['sum']) + '\n'
                    )
    msg.attach(body)

    noize_graph_part = createPartFormBuf(noize_graph_buf, 'noize.png')
    human_graph_part = createPartFormBuf(human_graph_buf, 'human.png')
    msg.attach(noize_graph_part)
    msg.attach(human_graph_part)

    server = smtplib.SMTP('smtp.gmail.com:587')
    # server.set_debuglevel(True)
    server.starttls()
    server.login(msg['From'], password)
    server.sendmail(msg['From'], msg['To'], msg.as_string())
    server.quit()


def createPartFormBuf(buf, name = 'anything.png'):
    part = MIMEBase('application', "octet-stream")
    part.set_payload(buf.read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="%s"' % name)
    return part