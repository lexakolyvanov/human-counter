import simple_http_server.server as server
import ctrl


def main(*args):
    server.start()

if __name__ == "__main__":
    main()


#  pip install -r requirements.txt
#  python app.py