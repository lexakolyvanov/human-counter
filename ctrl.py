import io

import matplotlib
matplotlib.use('Agg')

from simple_http_server import request_map, PathValue
from simple_http_server import Response
from datetime import datetime, timedelta
import sqlite3
import json
from sender import sendMessage
import matplotlib.pyplot as plt


TIME_FORMAT = '%Y-%m-%d %H:%M:%S'


@request_map("/noize/{data}", method="GET")
def my_ctrl3(data=PathValue()):
    if data is None:
        return Response(status_code=400)

    time = datetime.strftime(datetime.now(), TIME_FORMAT)
    print(time, data)

    conn = sqlite3.connect("mydatabase.db")  # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()

    cursor.execute("""INSERT INTO noize ("data", "created_at") VALUES ('""" + data + """', '""" + time + """')""")
    conn.commit()

    return Response(status_code=200)


@request_map("/human/{data}", method="GET")
def human(data=PathValue()):
    if data is None:
        return Response(status_code=400)

    time = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
    old_time = datetime.strftime(datetime.now() - timedelta(days=1), "%Y-%m-%d %H:%M:%S")
    print(time, data)

    conn = sqlite3.connect("mydatabase.db")  # или :memory: чтобы сохранить в RAM
    cursor = conn.cursor()

    cursor.execute("""SELECT data, count FROM human where datetime(created_at) > datetime('""" + old_time + """') ORDER BY created_at DESC LIMIT 1;""")

    base_data = cursor.fetchall()
    if len(base_data) == 0:
        old_data = "0"
        old_count = 0

    else:
        base_data = base_data[0]
        old_data = base_data[0]
        old_count = int(base_data[1])

    if (int(old_data) < int(data)):
        old_count += int(data) - int(old_data)

    cursor.execute(
        """INSERT INTO human ("data", "created_at", "count") VALUES ('""" + data + """', '""" + time + """', '""" + str(
            old_count) + """')""")
    conn.commit()

    return Response(status_code=200)


@request_map("/send", method="GET")
def send():
    time = datetime.strftime(datetime.now() - timedelta(days=1), "%Y-%m-%d %H:%M:%S")
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    sql = """SELECT * from noize where datetime(created_at) > datetime('""" + time + """');"""
    cursor.execute(sql)
    noize = cursor.fetchall()

    sql = """SELECT * from human where datetime(created_at) > datetime('""" + time + """');"""
    cursor.execute(sql)
    human_count = cursor.fetchall()

    # print(noize, human_count)
    noize_response = {"items": [x[0] for x in noize], "times": [x[1] for x in noize]}
    human_response = {"items": [x[0] for x in human_count], "times": [x[1] for x in human_count],
                      "count": [x[2] for x in human_count]}
    noize_graph_buf = drawGraph(noize_response['items'], noize_response['times'])
    human_graph_buf = drawGraph(human_response['items'], human_response['times'])

    message_data = generateData(noize_response['items'], human_response['items'], human_response['count'])

    sendMessage(message_data, noize_graph_buf, human_graph_buf)
    resp = json.dumps(message_data)
    return resp


def generateData(noize, human, human_count):
    noize = [int(item) for item in noize]
    human = [int(item) for item in human]
    human_count = [int(item) for item in human_count]

    message_data = {"noise": {"max": max(noize), "min": min(noize)},
                    "human": {"max": max(human), "min": min(human), "sum": max(human_count)}
                    }
    print(message_data)
    return message_data


def drawGraph(items, times):
    new_times = []
    new_items = [float(item) for item in items]
    for time in times:
        new_times.append(datetime.strptime(time, TIME_FORMAT))

    optimal_length = min(len(new_items), len(new_times))

    plt.plot(new_times[:optimal_length], new_items[:optimal_length])
    plt.gcf().autofmt_xdate()
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    plt.close()

    return buf
